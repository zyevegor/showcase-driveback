FROM node:7.6.0
WORKDIR /build/
COPY . .
RUN npm install
RUN npm run build


FROM node:7.6.0
WORKDIR /app/
COPY --from=0 /build/dist .
RUN npm install
EXPOSE 3000
CMD ["node", "babel-server.js"]