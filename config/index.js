if (process.env.NODE_ENV !== 'production')
  require('dotenv').load();

/**
 * @factory
 * @description config section
 * @type {{server: {port: number}, redis: {port: number, host: (*|string)}, middleware: {counterDrop: number}}}
 */
module.exports = {
  server: {
    port: parseInt(process.env.PORT) || 3000
  },
  redis: {
    port: parseInt(process.env.REDIS_PORT) || 6379,
    host: process.env.REDIS_HOST || 'localhost'
  },
  middleware: {
    counterDrop: parseInt(process.env.COUNTER_DROP_TIME) || 60
  }
};