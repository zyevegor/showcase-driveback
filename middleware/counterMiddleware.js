const visitCounterController = require('../controllers/visitCounterController');

/**
 * @function
 * @description middleware
 * @param req - the req object
 * @param res - the res object
 * @param next - next function
 * @return {Promise<void>}
 */
module.exports = async (req, res, next)=>{
  await visitCounterController.increment();
  next();
};
