const expect = require('chai').expect,
  config = require('../config'),
  redis = require('redis'),
  Promise = require('bluebird'),
  request = require('request-promise'),
  _ = require('lodash'),
  ctx = {};

Promise.promisifyAll(redis);

describe('server/api', function () {

  before(async () => {
    ctx.client = redis.createClient({host: config.redis.host, port: config.redis.port});
    await ctx.client.flushdbAsync();
  });

  after(async () => {
    await ctx.client.flushdbAsync();
    ctx.client.quit();
  });


  it('check status route', async () => {
    let response = await request({
      url: `http://localhost:${config.server.port}/counter/`,
      json: true
    });

    expect(response.count).to.equal(0);
  });


  it('try to obtain page random amount of times', async () => {

    ctx.requestAmount = _.random(20000, 30000);
    await Promise.map(new Array(ctx.requestAmount), async () => {
      await request({
        url: `http://localhost:${config.server.port}/page/`,
        json: true
      });
    }, {concurrency: 100})


  });

  it('check status route again', async () => {
    let response = await request({
      url: `http://localhost:${config.server.port}/counter/`,
      json: true
    });

    expect(response.count).to.equal(ctx.requestAmount);
  });

});
