const getCounterStatus = require('../services/counter/getCounterStatus');

module.exports = (router)=>{

  router.get('/', getCounterStatus);

};