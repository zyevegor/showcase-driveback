const requireAll = require('require-all'),
  express = require('express'),
  paths = requireAll({
    dirname: __dirname,
    filter: /(.+Routes)\.js$/,
    recursive: true,
    map: function (name) {
      return name.replace('Routes', '');
    }
  });

/**
 *
 * @param app - the express instance
 * @description the router registrator
 */

module.exports = (app) => {

  for (let path of Object.keys(paths)) {
    let router = express.Router();
    paths[path](router);
    app.use(`/${path}`, router);
  }

};
