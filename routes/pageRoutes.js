const getDefaultPageService = require('../services/pages/getDefaultPageService'),
  counterMiddleware = require('../middleware/counterMiddleware');

module.exports = (router) => {

  router.use(counterMiddleware);

  router.get('/', getDefaultPageService);

};