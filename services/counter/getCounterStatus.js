const visitCounterController = require('../../controllers/visitCounterController');

/**
 * @function
 * @description get visit count
 * @param req - the req object
 * @param res - the res object
 * @return {Promise<void>}
 */
module.exports = async (req, res) => {

  let count = await visitCounterController.getCount();

  res.send({count: count});
};
