/**
 * @factory
 * @description general messages
 * @type {{server: {started: string}}}
 */
module.exports = {
  server: {
    started: 'the server has started'
  }
};