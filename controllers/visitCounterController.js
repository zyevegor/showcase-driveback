const config = require('../config'),
  redis = require('redis'),
  Promise = require('bluebird'),
  uniqid = require('uniqid'),
  EventEmitter = require('events');

Promise.promisifyAll(redis);

/**
 * @class
 * @description the class for handling clients visits
 */
class VisitCounter extends EventEmitter {
  constructor() {
    super();
  }

  async openDb() {
    this.client = redis.createClient({host: config.redis.host, port: config.redis.port});
    this.client.on('error', err => {
      this.emit('error', err);
    });

  }


  async increment() {
    await this.client.setAsync(`counter:${uniqid()}`, 1, 'EX', config.middleware.counterDrop);
  }

  async getCount(){
    const keys = await this.client.keysAsync('counter:*');
    return keys.length;
  }

}


module.exports = new VisitCounter();