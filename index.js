const express = require('express'),
  config = require('./config'),
  bunyan = require('bunyan'),
  routes = require('./routes'),
  messages = require('./factories/messages/general'),
  visitCounterController = require('./controllers/visitCounterController'),
  log = bunyan.createLogger({name: 'server.index'}),
  app = express();

/**
 * @function
 * @description entry point
 * @return {Promise<void>}
 */
const init = async () => {

  await visitCounterController.openDb();
  routes(app);

  app.listen(config.server.port, () =>
    log.info(messages.server.started));
};

module.exports = init().catch(err => {
  log.error(err);
  process.exit(0);
});