# showcase-driveback

The service for tracking page views

### Installation

Just clone the repo, do 'npm install', set your .env - and you are ready to go.

### Run
Just start the index.js file from the project root:
```
node .
```

#### About
This service is responsible for tracking pageviews for the past time (please check config section about COUNTER_DROP_TIME param)

#### Endpoints

There are 2 main endpoints:

1) The page endpoint
```
/page
````
 Which should return the JSON object:
```
{"status": 1}
```

2. The counter (which displays the amount of requests, emitted in last time (the COUNTER_DROP_TIME you specify by yourself))
```
/counter
```
 Which should return the JSON object:
```
{"count": 5}
```

 #### The flow
 When user makes an API call to /page, the middleware catch this event, and add the new record to the REDIS.

 Now let's assume, that we want to track only total amount of page loads for the last minute. We set the COUNTER_DROP_TIME=60 (seconds). Now, when users will access this page, new records appear in REDIS, but with TTL of 60 seconds. Next we can call /counter route, which should return the total amount of calls to /page for the past minute. After TTL expire, the record is dropped. In this way, we can easely track the total amount of API calls for the past minute, as all records, which have been added earlier - will be dropped.


##### сonfigure your .env

To apply your configuration, create a .env file in root folder of repo (in case it's not present already).
Below is the expamle configuration:

```
PORT=3000
REDIS_PORT=6379
REDIS_HOST=localhost
COUNTER_DROP_TIME=60
=
```

The options are presented below:

| name | description|
| ------ | ------ |
| PORT   | rest port
| REDIS_PORT   | REDIS port
| REDIS_HOST   | REDIS host
| COUNTER_DROP_TIME   | the TTL, when records are going to expire

#### Old Node.js versions support

In order to bring the compability with old node.js versions, we have introduced the babel as optional feature.

You can create a build with the following command:
```
npm run build
```

This will transpile the ES6 code to the ES5, and put it into dist folder

Then you can run your build by typing:
```
npm run serve
```


